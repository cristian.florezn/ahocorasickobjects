package model;

import java.util.Hashtable;

public class Node {
	
	private Integer id;
	private Hashtable<Character, Node> children;
	
	public Node(Integer id) {
		this.id = id;
		children = new Hashtable<Character, Node>();
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void addChild(Character c, Node child) {
		children.put(c, child);
	}
	
	public Node getChild(Character c) {
		return this.id == 0 ? (children.get(c) == null ? this : children.get(c)) : children.get(c);
	}
	
	public boolean hasTransition(Character c) {
		return this.id == 0 ? true : children.containsKey(c);
	}
	
	public boolean hasChild(Node child) {
		return children.contains(child);
	}
	
	public Hashtable<Character, Node> getChildren() {
		return this.children;
	}

	@Override
	public String toString() {
		return "Node [id=" + id + ", children=" + children + "]";
	}
}
