package model;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class AhoCorasick {
	
	private Node root;
	private int nodesCounter;
	private ArrayList<String> keyWords;
	private Hashtable<Node, ArrayList<String>> output;
	private Hashtable<Node, Node> failure;
	
	public AhoCorasick() {
		root = new Node(0);
		nodesCounter = 1;
		keyWords = new ArrayList<String>();
		output = new Hashtable<Node, ArrayList<String>>();
		failure = new Hashtable<Node, Node>();
	}
	
	public ArrayList<String> getKeyWords() {
		return this.keyWords;
	}
	
	public void addKeyWord(String keyWord) {
		keyWord = keyWord.toUpperCase();
		if (!keyWords.contains(keyWord)) {
			char[] keyWordArr = keyWord.toCharArray();
			Node state = root;
			for (Character c: keyWordArr) {
				if (state.hasTransition(c) && !state.equals(state.getChild(c))) {
					state = state.getChild(c);
				} else {
					Node newState = new Node(nodesCounter);
					nodesCounter++;
					state.addChild(c, newState);
					state = newState;
				}
			}
			ArrayList<String> outputs = new ArrayList<String>();
			outputs.add(keyWord);
			output.put(state, outputs);
			updateFailure();
			keyWords.add(keyWord);
		}
	}
	
	private void updateFailure() {
		Queue<Node> queue = new LinkedList<Node>();
		
		for (Map.Entry<Character, Node> entry : root.getChildren().entrySet()) {
			Node child = entry.getValue();
			queue.add(child);
			failure.put(child, root);
		}
		
		while (!queue.isEmpty()) {
			Node state = queue.poll();
			
			for (Map.Entry<Character, Node> entry : state.getChildren().entrySet()) {
				Character transition = entry.getKey();
				Node nextState = entry.getValue();
				queue.add(nextState);
				Node stateF = failure.get(state);
				while (!stateF.hasTransition(transition)) {
					stateF = failure.get(stateF);
				}
				stateF = stateF.getChild(transition);
				failure.put(nextState, stateF);
				if (output.containsKey(stateF)) {
					ArrayList<String> outputs = output.containsKey(nextState) ? output.get(nextState) : new ArrayList<String>();
					for (String out : output.get(stateF)) {
						if (!outputs.contains(out)) {
							outputs.add(out);
						}
					}
					output.put(nextState, outputs);
				}
			}
		}
	}
	
	public Hashtable<String, ArrayList<Integer>> ahoCorasick(String text) {
		Hashtable<String, ArrayList<Integer>> answer = new Hashtable<String, ArrayList<Integer>>();
		text = text.toUpperCase();
		int n = text.length();
		Node state = root;
		int index;
		for (int i=0; i<n; i++) {
			Character c = text.charAt(i);
			while (!state.hasTransition(c)) {
				state = failure.get(state);
			}
			state = state.getChild(c);
			if (output.containsKey(state)) {
				ArrayList<String> outputs = output.get(state);
				for (String out : outputs) {
					ArrayList<Integer> indexes = answer.containsKey(out) ? answer.get(out) : new ArrayList<Integer>();
					index = i - out.length() + 1;
					indexes.add(index);
					answer.put(out, indexes);
				}
			}
		}
		return answer;
	}

}
