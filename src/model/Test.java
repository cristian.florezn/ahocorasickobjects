package model;

public class Test {
	
	public static void main(String[] args) {
		AhoCorasick aC = new AhoCorasick();
		aC.addKeyWord("Clave");
		aC.addKeyWord("Ver");
		aC.addKeyWord("Ave");
		aC.addKeyWord("Clase");
		String texto = "clavaverclaseclaveraver";
		System.out.println("Palabras Clave: " + aC.getKeyWords());
		System.out.println("Buscar en: " + texto);
		System.out.println(aC.ahoCorasick(texto));
	}

}
