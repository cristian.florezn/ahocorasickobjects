package main;

import java.util.Scanner;

import model.AhoCorasick;

public class Main {
	
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		AhoCorasick aC = new AhoCorasick();
		
		showMenu();
		System.out.println("Option: ");
		int option = scanner.nextInt();
		String keyWord, text;
		while (option != 5) {
			if (option == 1) {
				System.out.println("Insert Keyword:");
				keyWord = scanner.next();
				aC.addKeyWord(keyWord);
				System.out.println("Keyword added.");
			} else if (option == 2) {
				System.out.println("Keywords: " + aC.getKeyWords());
			} else if (option == 3) {
				System.out.println("Insert text to use:");
				text = scanner.next();
				System.out.println(aC.ahoCorasick(text));
			} else if (option == 4) {
				aC = new AhoCorasick();
			} else {
				System.out.println("Option not valid.");
			}
			
			showMenu();
			System.out.println("Option: ");
			option = scanner.nextInt();
		}
		System.out.println("Have a nice day.");
		
		
	}
	
	private static void showMenu() {
		System.out.println("Type one of the following options.");
		System.out.println("1. Add a key word.");
		System.out.println("2. See key words.");
		System.out.println("3. Use Aho Corasick.");
		System.out.println("4. Clean key words.");
		System.out.println("5. Exit.");
	}

}
